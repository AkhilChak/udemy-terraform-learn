variable "access_key_var" {
  description = "access key aws"
}
variable "secret_key_var" {
  description = "secret key aws"
}

provider "aws"{
    region = "ap-south-1"
    access_key = var.access_key_var
    secret_key = var.secret_key_var
}

variable "subnet_cidr_block" {
  description = "subnet cidr block"
}

variable "vpc_cidr_block" {
  description = "vpc cidr block"
}

resource "aws_vpc" "development-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name: "development",
        vpc_env: "dev"
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = "ap-south-1c" 
    tags = {
        Name: "subnet-1-dev"
    }
}